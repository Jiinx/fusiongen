<!DOCTYPE html>
<html lang="en">
<head>
    <style type="text/css">
        body {
            background-color: #eee;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
        }

        h1 {
            font-size: 24px;
            font-weight: normal;
            color: #666;
        }

        .err {
            color: red;
        }

        .noerr {
            color: green;
        }
    </style>

</head>
<body>
<h1>Server Settings</h1>
<ul>
    <li>Operating System: <?php echo php_uname(); ?></li>
    <li>HTTP Server: <?php echo $_SERVER['SERVER_SOFTWARE']; ?></li>
    <li>PHP Version: <?php echo phpversion(); ?></li>
</ul>
<h1>PHP Modules</h1>
<ul>
    <?php
    $pmods = ['mysqli', 'curl', 'openssl', 'soap', 'gd', 'mbstring', 'json'];

    foreach ($pmods as $key => $val) {
        if (extension_loaded($val)) {
            echo '<li>php_' . $val . ': <span class="noerr">Installed</span></li>';
        } else {
            echo '<li>php_' . $val . ': <span class="err">Not Installed</span></li>';
        }
    } ?>
</ul>
<h1>Connection Status:</h1>
<?php
define('BASEPATH', __DIR__);
require_once '../application/config/database.php'; 
?>
<ul>
    <li>CMS Database</li>
    <?php
    $server = $db["cms"]["hostname"] . ':' . $db["cms"]["port"];
    $user = $db["cms"]["username"];
    $pass = $db["cms"]["password"];
    $dbname = $db["cms"]["database"];

    $conn = mysqli_connect($server, $user, $pass, $dbname); ?>
    <ul>
        <li>Status: <?php
            if ($conn->connect_error) {
                echo '<span class="err">Connection Error </span>( ' . $conn->connect_error . ')';
            } else {
            echo '<span class="noerr">Connected</span>';
            ?>
        </li>
        <li>Accounts: <?php
            $query = "SELECT * FROM account_data";

            if ($result = mysqli_query($conn, $query)) {
                $rows = mysqli_num_rows($result);
                echo $rows;

                mysqli_free_result($result);
            }

            ?></li>
        <li>Realms: <?php
            $query = "SELECT * FROM realms";

            if ($result = mysqli_query($conn, $query)) {
                $row = mysqli_fetch_array($result);

                echo mysqli_num_rows($result);
            } ?></li>
        <ul>
            <li>Realm: <?php echo $row['id']; ?></li>
            <li>Status: <?php
                $connect = fsockopen($row['hostname'], $row['realm_port'], $errno, $errstr, 1.5);
                echo $connect ? '<span class="noerr">Online</span>' : '<span class="err">Offline</span>';

                ?>
            </li>
            <li>Character Database: <?php
                $cdb = $row['char_database'];
                echo $cdb;
                ?>
            </li>
            <li>World Database: <?php
                $wdb = $row['world_database'];
                echo $wdb;
                ?>
            </li>


        </ul>

        <?php mysqli_close($conn);
        } ?>
    </ul>
    <li>Emulation Database:</li>
    <?php
    $server = $db["account"]["hostname"] . ':' . $db["account"]["port"];
    $user = $db["account"]["username"];
    $pass = $db["account"]["password"];
    $dbname = $db["account"]["database"];

    $conn = mysqli_connect($server, $user, $pass, $dbname); ?>
    <ul>
        <li>Status: <?php
            if ($conn->connect_error) {
                echo '<span class="err">Connection Error </span>( ' . $conn->connect_error . ')';
            } else {
                echo '<span class="noerr">Connected</span>';
            }?>
        </li>
        <li>Auth Database:</li>
        <ul>
            <li>Accounts: <?php
                $query = "SELECT * FROM account";

                if ($result = mysqli_query($conn, $query)) {
                    $rows = mysqli_num_rows($result);
                    echo $rows;

                    mysqli_free_result($result);
                }

                ?></li>
        </ul>
        <li>Character Database:</li>
        <ul>
            <li>Characters: <?php
                mysqli_select_db($conn, $cdb);
                $query = "SELECT * FROM characters";

                if ($result = mysqli_query($conn, $query)) {
                    $rows = mysqli_num_rows($result);
                    echo $rows;

                    mysqli_free_result($result);
                }

                ?></li>
        </ul>
        <li>World Database:</li>
        <ul>
            <li>Version: <?php
                mysqli_select_db($conn, $wdb);
                $query = "SELECT * FROM version";

                if ($result = mysqli_query($conn, $query)) {
                    $row = mysqli_fetch_array($result);

               // if ($result = mysqli_query($conn, $query)) {
                    echo $row['core_version'];

                    mysqli_free_result($result);
                }

                ?></li>
        </ul>
    </ul>
</ul>


</body>
<footer>
</footer>
</html>

